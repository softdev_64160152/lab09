/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.arisa.lab9.component;

import com.arisa.lab09.model.Product;

/**
 *
 * @author Asus
 */
public interface BuyProductable {
    public void buy(Product product, int qty);
}
