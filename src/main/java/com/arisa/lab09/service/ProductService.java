/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.arisa.lab09.service;

import com.arisa.lab09.dao.ProductDao;
import com.arisa.lab09.model.Product;
import java.util.ArrayList;

/**
 *
 * @author Asus
 */
public class ProductService {
    private ProductDao productDao = new ProductDao();
    public ArrayList<Product> getProductsOrderbyName(){
        return (ArrayList<Product>) productDao.getAll(" product_name ASC");
    }
}
